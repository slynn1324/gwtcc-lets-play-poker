package com.gwtcrashcourse.letsplaypoker.engine.res;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility wrapper around a StringBuffer to facilitate writing formatted source code.
 * @author req77608
 *
 */
public class SourceWriter {

    private StringBuffer sb = new StringBuffer();

    private int indent = 1;

    private static final int SPACES_PER_INDENT = 2;
    
    private String packageName;
    private String className;
    private List<String> imports = new ArrayList<String>();
    private List<String> classAnnotations = new ArrayList<String>();
    private String parentClass;

    private String classComment = null;
    private boolean isInterface;
    
    public SourceWriter(String packageName, String className){
    	this(packageName, className, false);
    }
    
    public SourceWriter(String packageName, String className, boolean isInterface){
        this.packageName = packageName;
        this.className = className;
        this.isInterface = isInterface;
    }
    
    public void setClassComment(String comment){
        classComment = comment;
    }
    
    public void setParentClass(String className){
        this.parentClass = className;
    }
    
    public void addClassAnnotation(String annotation){
        classAnnotations.add(annotation);
    }
    
    public void addImport(String className){
        imports.add(className);
    }
    
    public void println(){
    	sb.append("\n");
    }
    
    public void println(String str) {
        int numSpaces = indent * SPACES_PER_INDENT;
        for (int i = 0; i < numSpaces; ++i) {
            sb.append(" ");
        }
        sb.append(str).append("\n");
    }

    public void print(String str) {
        sb.append(str);
    }

    public void indent() {
        ++indent;
    }

    public void outdent() {
        --indent;
        if (indent < 0) {
            indent = 0;
        }
    }

    @Override
    public String toString() {
        
        StringBuilder src = new StringBuilder();
        src.append("package ").append(packageName).append(";\n\n");

        for ( String i : imports ){
            src.append("import ").append(i).append(";\n");
        }
        src.append("\n");
        
        if ( classComment != null ){
            src.append("/**\n");
            src.append(classComment);
            src.append("\n**/\n");
        }
        
        for ( String a : classAnnotations){
            src.append(a).append("\n");
        }
        
        src.append("public ");
        
        if ( isInterface ){
        	src.append("interface ");
        } else {
        	src.append("class ");
        }
        
        src.append(className);
        if ( isNotEmpty(parentClass) ){
            src.append(" extends " + parentClass);
        }
        src.append(" {\n");
        
        src.append(sb.toString());
        
        src.append("\n}\n");
        
        return src.toString();
    }
    
    // clone StringUtils logic to avoid dependency on StringUtils
    
    protected static boolean isEmpty(String str){
    	if ( str == null ){
    		return true;
    	}
    	
    	if ( str.length() == 0) {
    		return true;
    	}
    	
    	return false;
    }
    
    protected static boolean isNotEmpty(String str){
    	return !isEmpty(str);
    }
}
