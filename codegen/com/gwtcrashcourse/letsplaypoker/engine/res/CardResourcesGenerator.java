package com.gwtcrashcourse.letsplaypoker.engine.res;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Custom generator to create ImageResorces. Currently hard-coded
 * for the CardImages bundle, but could (and should) easily be abstracted for any folder
 * of images.
 * 
 * @author req77608
 *
 */
public class CardResourcesGenerator {

	public static void main(String[] args) throws IOException{
		
		SourceWriter src = new SourceWriter("com.gwtcrashcourse.letsplaypoker.engine.res", "CardResources", true);
		
		src.addImport("com.google.gwt.core.client.GWT");
		src.addImport("com.google.gwt.resources.client.ClientBundle");
		src.addImport("com.google.gwt.resources.client.ImageResource");
		src.addImport("java.util.Map");
		src.addImport("java.util.HashMap");
		
		src.setParentClass("ClientBundle");
		
		src.println("public static final CardResources INSTANCE = GWT.create(CardResources.class);");
		src.println();
		
		File dir = new File("src/com/gwtcrashcourse/letsplaypoker/engine/res/cardimages");
		
		List<String> images = new ArrayList<String>();
		
		if ( dir.isDirectory() ){
			File[] files = dir.listFiles();
			
			for ( File file : files ){
				if ( file.getName().endsWith(".png") ){
					
					String name = file.getName();
					String baseName = name.substring(0, name.length()-4);
					
					
					if ( baseName.substring(0,1).matches("[0-9_]") ){
						baseName = "_" + baseName;
					}
					
					images.add(baseName);
					
					src.println("@Source(\"cardimages/" + name + "\")");
					src.println("ImageResource " + baseName + "();");
					src.println();
				}
			}
			
		}
		
		src.println("// utility to account for no reflection");
		src.println("public static class Lookup{");
		src.indent();
		src.println("private static final Map<String, ImageResource> map = new HashMap<String,ImageResource>();");
		
		src.println("static{");
		src.indent();
		for ( String image : images ){
			
			String origName = image;
			if ( origName.startsWith("_") ){
				origName = origName.substring(1);
			}
			
			src.println("map.put(\"" + origName + "\", CardResources.INSTANCE." + image + "());");
		}
		src.outdent();
		src.println("}");
		
		src.println("public static ImageResource get(String name){");
		src.indent();
		src.println("return map.get(name);");
	    src.outdent();
	    src.println("}");
		
		
		src.outdent();
		src.println("}");
		
		File f = new File("src/com/gwtcrashcourse/letsplaypoker/engine/res/CardResources.java");
		FileOutputStream fout = new FileOutputStream(f);
		BufferedWriter bufOut = new BufferedWriter(new OutputStreamWriter(fout));
		bufOut.write(src.toString());
		bufOut.close();
		fout.close();
		
		System.out.println("finished.");
	}
}
