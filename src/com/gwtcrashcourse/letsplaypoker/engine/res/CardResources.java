package com.gwtcrashcourse.letsplaypoker.engine.res;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import java.util.Map;
import java.util.HashMap;

public interface CardResources extends ClientBundle {
  public static final CardResources INSTANCE = GWT.create(CardResources.class);

  @Source("cardimages/10_of_clubs.png")
  ImageResource _10_of_clubs();

  @Source("cardimages/10_of_diamonds.png")
  ImageResource _10_of_diamonds();

  @Source("cardimages/10_of_hearts.png")
  ImageResource _10_of_hearts();

  @Source("cardimages/10_of_spades.png")
  ImageResource _10_of_spades();

  @Source("cardimages/2_of_clubs.png")
  ImageResource _2_of_clubs();

  @Source("cardimages/2_of_diamonds.png")
  ImageResource _2_of_diamonds();

  @Source("cardimages/2_of_hearts.png")
  ImageResource _2_of_hearts();

  @Source("cardimages/2_of_spades.png")
  ImageResource _2_of_spades();

  @Source("cardimages/3_of_clubs.png")
  ImageResource _3_of_clubs();

  @Source("cardimages/3_of_diamonds.png")
  ImageResource _3_of_diamonds();

  @Source("cardimages/3_of_hearts.png")
  ImageResource _3_of_hearts();

  @Source("cardimages/3_of_spades.png")
  ImageResource _3_of_spades();

  @Source("cardimages/4_of_clubs.png")
  ImageResource _4_of_clubs();

  @Source("cardimages/4_of_diamonds.png")
  ImageResource _4_of_diamonds();

  @Source("cardimages/4_of_hearts.png")
  ImageResource _4_of_hearts();

  @Source("cardimages/4_of_spades.png")
  ImageResource _4_of_spades();

  @Source("cardimages/5_of_clubs.png")
  ImageResource _5_of_clubs();

  @Source("cardimages/5_of_diamonds.png")
  ImageResource _5_of_diamonds();

  @Source("cardimages/5_of_hearts.png")
  ImageResource _5_of_hearts();

  @Source("cardimages/5_of_spades.png")
  ImageResource _5_of_spades();

  @Source("cardimages/6_of_clubs.png")
  ImageResource _6_of_clubs();

  @Source("cardimages/6_of_diamonds.png")
  ImageResource _6_of_diamonds();

  @Source("cardimages/6_of_hearts.png")
  ImageResource _6_of_hearts();

  @Source("cardimages/6_of_spades.png")
  ImageResource _6_of_spades();

  @Source("cardimages/7_of_clubs.png")
  ImageResource _7_of_clubs();

  @Source("cardimages/7_of_diamonds.png")
  ImageResource _7_of_diamonds();

  @Source("cardimages/7_of_hearts.png")
  ImageResource _7_of_hearts();

  @Source("cardimages/7_of_spades.png")
  ImageResource _7_of_spades();

  @Source("cardimages/8_of_clubs.png")
  ImageResource _8_of_clubs();

  @Source("cardimages/8_of_diamonds.png")
  ImageResource _8_of_diamonds();

  @Source("cardimages/8_of_hearts.png")
  ImageResource _8_of_hearts();

  @Source("cardimages/8_of_spades.png")
  ImageResource _8_of_spades();

  @Source("cardimages/9_of_clubs.png")
  ImageResource _9_of_clubs();

  @Source("cardimages/9_of_diamonds.png")
  ImageResource _9_of_diamonds();

  @Source("cardimages/9_of_hearts.png")
  ImageResource _9_of_hearts();

  @Source("cardimages/9_of_spades.png")
  ImageResource _9_of_spades();

  @Source("cardimages/ace_of_clubs.png")
  ImageResource ace_of_clubs();

  @Source("cardimages/ace_of_diamonds.png")
  ImageResource ace_of_diamonds();

  @Source("cardimages/ace_of_hearts.png")
  ImageResource ace_of_hearts();

  @Source("cardimages/ace_of_spades.png")
  ImageResource ace_of_spades();

  @Source("cardimages/black_joker.png")
  ImageResource black_joker();

  @Source("cardimages/card_back.png")
  ImageResource card_back();

  @Source("cardimages/jack_of_clubs.png")
  ImageResource jack_of_clubs();

  @Source("cardimages/jack_of_diamonds.png")
  ImageResource jack_of_diamonds();

  @Source("cardimages/jack_of_hearts.png")
  ImageResource jack_of_hearts();

  @Source("cardimages/jack_of_spades.png")
  ImageResource jack_of_spades();

  @Source("cardimages/king_of_clubs.png")
  ImageResource king_of_clubs();

  @Source("cardimages/king_of_diamonds.png")
  ImageResource king_of_diamonds();

  @Source("cardimages/king_of_hearts.png")
  ImageResource king_of_hearts();

  @Source("cardimages/king_of_spades.png")
  ImageResource king_of_spades();

  @Source("cardimages/queen_of_clubs.png")
  ImageResource queen_of_clubs();

  @Source("cardimages/queen_of_diamonds.png")
  ImageResource queen_of_diamonds();

  @Source("cardimages/queen_of_hearts.png")
  ImageResource queen_of_hearts();

  @Source("cardimages/queen_of_spades.png")
  ImageResource queen_of_spades();

  @Source("cardimages/red_joker.png")
  ImageResource red_joker();

  // utility to account for no reflection
  public static class Lookup{
    private static final Map<String, ImageResource> map = new HashMap<String,ImageResource>();
    static{
      map.put("10_of_clubs", CardResources.INSTANCE._10_of_clubs());
      map.put("10_of_diamonds", CardResources.INSTANCE._10_of_diamonds());
      map.put("10_of_hearts", CardResources.INSTANCE._10_of_hearts());
      map.put("10_of_spades", CardResources.INSTANCE._10_of_spades());
      map.put("2_of_clubs", CardResources.INSTANCE._2_of_clubs());
      map.put("2_of_diamonds", CardResources.INSTANCE._2_of_diamonds());
      map.put("2_of_hearts", CardResources.INSTANCE._2_of_hearts());
      map.put("2_of_spades", CardResources.INSTANCE._2_of_spades());
      map.put("3_of_clubs", CardResources.INSTANCE._3_of_clubs());
      map.put("3_of_diamonds", CardResources.INSTANCE._3_of_diamonds());
      map.put("3_of_hearts", CardResources.INSTANCE._3_of_hearts());
      map.put("3_of_spades", CardResources.INSTANCE._3_of_spades());
      map.put("4_of_clubs", CardResources.INSTANCE._4_of_clubs());
      map.put("4_of_diamonds", CardResources.INSTANCE._4_of_diamonds());
      map.put("4_of_hearts", CardResources.INSTANCE._4_of_hearts());
      map.put("4_of_spades", CardResources.INSTANCE._4_of_spades());
      map.put("5_of_clubs", CardResources.INSTANCE._5_of_clubs());
      map.put("5_of_diamonds", CardResources.INSTANCE._5_of_diamonds());
      map.put("5_of_hearts", CardResources.INSTANCE._5_of_hearts());
      map.put("5_of_spades", CardResources.INSTANCE._5_of_spades());
      map.put("6_of_clubs", CardResources.INSTANCE._6_of_clubs());
      map.put("6_of_diamonds", CardResources.INSTANCE._6_of_diamonds());
      map.put("6_of_hearts", CardResources.INSTANCE._6_of_hearts());
      map.put("6_of_spades", CardResources.INSTANCE._6_of_spades());
      map.put("7_of_clubs", CardResources.INSTANCE._7_of_clubs());
      map.put("7_of_diamonds", CardResources.INSTANCE._7_of_diamonds());
      map.put("7_of_hearts", CardResources.INSTANCE._7_of_hearts());
      map.put("7_of_spades", CardResources.INSTANCE._7_of_spades());
      map.put("8_of_clubs", CardResources.INSTANCE._8_of_clubs());
      map.put("8_of_diamonds", CardResources.INSTANCE._8_of_diamonds());
      map.put("8_of_hearts", CardResources.INSTANCE._8_of_hearts());
      map.put("8_of_spades", CardResources.INSTANCE._8_of_spades());
      map.put("9_of_clubs", CardResources.INSTANCE._9_of_clubs());
      map.put("9_of_diamonds", CardResources.INSTANCE._9_of_diamonds());
      map.put("9_of_hearts", CardResources.INSTANCE._9_of_hearts());
      map.put("9_of_spades", CardResources.INSTANCE._9_of_spades());
      map.put("ace_of_clubs", CardResources.INSTANCE.ace_of_clubs());
      map.put("ace_of_diamonds", CardResources.INSTANCE.ace_of_diamonds());
      map.put("ace_of_hearts", CardResources.INSTANCE.ace_of_hearts());
      map.put("ace_of_spades", CardResources.INSTANCE.ace_of_spades());
      map.put("black_joker", CardResources.INSTANCE.black_joker());
      map.put("card_back", CardResources.INSTANCE.card_back());
      map.put("jack_of_clubs", CardResources.INSTANCE.jack_of_clubs());
      map.put("jack_of_diamonds", CardResources.INSTANCE.jack_of_diamonds());
      map.put("jack_of_hearts", CardResources.INSTANCE.jack_of_hearts());
      map.put("jack_of_spades", CardResources.INSTANCE.jack_of_spades());
      map.put("king_of_clubs", CardResources.INSTANCE.king_of_clubs());
      map.put("king_of_diamonds", CardResources.INSTANCE.king_of_diamonds());
      map.put("king_of_hearts", CardResources.INSTANCE.king_of_hearts());
      map.put("king_of_spades", CardResources.INSTANCE.king_of_spades());
      map.put("queen_of_clubs", CardResources.INSTANCE.queen_of_clubs());
      map.put("queen_of_diamonds", CardResources.INSTANCE.queen_of_diamonds());
      map.put("queen_of_hearts", CardResources.INSTANCE.queen_of_hearts());
      map.put("queen_of_spades", CardResources.INSTANCE.queen_of_spades());
      map.put("red_joker", CardResources.INSTANCE.red_joker());
    }
    public static ImageResource get(String name){
      return map.get(name);
    }
  }

}
