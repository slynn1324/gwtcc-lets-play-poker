package com.gwtcrashcourse.letsplaypoker.engine.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.gwtcrashcourse.letsplaypoker.engine.Card;
import com.gwtcrashcourse.letsplaypoker.engine.Rank;
import com.gwtcrashcourse.letsplaypoker.engine.Suit;
import com.gwtcrashcourse.letsplaypoker.engine.res.CardResources;

public class CardView extends Composite {

	private static CardUiBinder uiBinder = GWT.create(CardUiBinder.class);

	interface CardUiBinder extends UiBinder<Widget, CardView> { /* marker */
	}
	
	static{
		// prefetch this image, so it's cached early
		Image.prefetch(CardResources.INSTANCE.card_back().getSafeUri());
	}
	
	@UiField Image cardImage;
	
	public CardView() {
		initWidget(uiBinder.createAndBindUi(this));
		cardImage.setUrl(CardResources.INSTANCE.card_back().getSafeUri());
	}
	
	public void setCard(Card c){
		if ( c == null || c.getRank() == null || c.getSuit() == null ){
			cardImage.setUrl(CardResources.INSTANCE.card_back().getSafeUri());
		} else {
			ImageResource r = CardResources.Lookup.get(getImageName(c));
			cardImage.setUrl(r.getSafeUri());
		}
	}
	
	protected static String getImageName(Card c){
		return getRankImageName(c.getRank()) + "_of_" + getSuitImageName(c.getSuit());
	}
	
	protected static String getSuitImageName(Suit s){
		return s.toString().toLowerCase() + "s";
	}
	
	protected static String getRankImageName(Rank r){
		if ( r.getIntValue() < Rank.JACK.getIntValue() ){
			return Integer.toString(r.getIntValue());
		} 
		return r.toString().toLowerCase();
	}

}
