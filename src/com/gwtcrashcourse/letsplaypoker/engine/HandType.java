package com.gwtcrashcourse.letsplaypoker.engine;

/**
 * Enumerates the possible hand types.
 * 
 */
public enum HandType {
	ROYAL_FLUSH(10, "Royal Flush"), 
	STRAIGHT_FLUSH(9, "Straight Flush"), 
	FOUR_OF_A_KIND(8, "Four of a Kind"), 
	FULL_HOUSE(7, "Full House"), 
	FLUSH(6, "Flush"), 
	STRAIGHT(5, "Straight"), 
	THREE_OF_A_KIND(4, "Three of a Kind"), 
	TWO_PAIR(3, "Two Pair"), 
	ONE_PAIR(2, "One Pair"), 
	HIGH_CARD(1, "High Card"),
	NOT_SCORED(0, "");
	
	private int rank;
	private String nice;
	
	private HandType(int rank, String nice){
		this.rank = rank;
		this.nice = nice;
	}
	
	public int getRank(){
		return rank;
	}
	
	public String getNiceString(){
		return nice;
	}
}
