package com.gwtcrashcourse.letsplaypoker.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Internally used evaluator class that determines the HandType from a group of cards.
 * @author slynn1324
 *
 */
public class NaiveHandEvaluator {

	private List<Card> cards;
	private List<Card> sortedCards = new ArrayList<Card>();
	private Map<Rank,List<Card>> groupedByRank = new HashMap<Rank,List<Card>>();
	
	private Boolean isFlush = null;
	private Boolean isStraight = null;
	
	private HandType handType = HandType.NOT_SCORED;
	private List<Card> cardsUsedInScore;
	
	public NaiveHandEvaluator(List<Card> cards){
		this.cards = cards;
	}
	
	public HandType getHandType(){
		return handType;
	}
	
	public List<Card> getCardsUsedInScore(){
		return cardsUsedInScore;
	}
	
	
	/**
	 * Execute the scoring algorithm. The properties of this instance will
	 * be populated appropriately.
	 */
	public void score(){
		sortCards();
		groupCards();
		
		boolean match = isRoyalFlush();
		if (!match){
			match = isStraightFlush();
		} 
		if (!match){
			match = isFourOfAKindOrFullHouse();
		}
		if (!match){
			match = isFlush();
		}
		if (!match){
			match = isStraight();
		}
		if (!match){
			match = isThreeOfAKind();
		}
		if (!match){
			match = isTwoPairOrOnePairOrHighCard();
		}
		
	}
	
	private void sortCards(){
		sortedCards.addAll(cards);
		Collections.sort(sortedCards);
	}
	
	private void groupCards(){
		for ( Card c : cards ){
			List<Card> cards = groupedByRank.get(c.getRank());
			if ( cards == null ){
				cards = new ArrayList<Card>();
				groupedByRank.put(c.getRank(), cards);
			}
			cards.add(c);
		}
	}
	
	private void setHandTypeIfHigherRank(HandType handType, List<Card> cards){
		if ( handType.getRank() > this.handType.getRank() ){
			this.handType = handType;
			cardsUsedInScore = cards;
		}
	}
	
	private boolean isRoyalFlush(){
		boolean result = false;
		// if it's a straight flush and high card is ace
		if ( isStraightFlush() && sortedCards.get(0).getRank() == Rank.ACE ){
			result = true;
			setHandTypeIfHigherRank(HandType.ROYAL_FLUSH, sortedCards);
		}
		return result;
	}
	
	private boolean isStraightFlush(){
		boolean result = false;
		if (isStraight() && isFlush()){
			result = true;
			setHandTypeIfHigherRank(HandType.STRAIGHT_FLUSH, sortedCards);
		} 
		return result;
	}
	
	private boolean isFourOfAKindOrFullHouse(){
		boolean result = false;
		
		// if it's only 2 numbers, it's either 4 of a kind or a full house
		if ( groupedByRank.size() == 2 ){
			
			for ( List<Card> group : groupedByRank.values() ){
			
				if ( group.size() == 4 ){
					setHandTypeIfHigherRank(HandType.FOUR_OF_A_KIND, group);
				} else if ( group.size() == 3){
					setHandTypeIfHigherRank(HandType.FULL_HOUSE, sortedCards);
				}
				
			}
			
			result = true;
		}
		
		return result;
	}
	
	private boolean isThreeOfAKind(){
		boolean result = false;
		
		
		if ( groupedByRank.size() == 3){
			
			for ( List<Card> threeOfAKind : groupedByRank.values() ){
				if ( threeOfAKind.size() == 3 ){
					result = true;
					setHandTypeIfHigherRank(HandType.THREE_OF_A_KIND, threeOfAKind);
				}
			}
		}
		
		return result;
	}
	
	private boolean isTwoPairOrOnePairOrHighCard(){
		
		List<List<Card>> pairs = new ArrayList<List<Card>>();
		
		
		for ( List<Card> pair : groupedByRank.values() ){
			if ( pair.size() == 2 ){
				pairs.add(pair);
			}
		}
	
		
		if ( pairs.size() == 2 ){
			List<Card> cardsUsed = new ArrayList<Card>();
			cardsUsed.addAll(pairs.get(0));
			cardsUsed.addAll(pairs.get(1));
			setHandTypeIfHigherRank(HandType.TWO_PAIR, cardsUsed);
		} else if (pairs.size() == 1){
			setHandTypeIfHigherRank(HandType.ONE_PAIR, pairs.get(0));
		} else {
			setHandTypeIfHigherRank(HandType.HIGH_CARD, new ArrayList<Card>(sortedCards.subList(0, 1)));
		}
		
		return true;
	}
	
	
	private boolean isStraight(){
		if ( isStraight != null ){
			return isStraight;
		}
		
		isStraight = false;
		
		// must have 5 entries in the map to be unique
		if ( groupedByRank.size() == cards.size() ){
			isStraight = true;
			// iterate through the cards, each 1 must be 1 smaller than the last
			Card lastCard = sortedCards.get(0);
			for( int i = 1; i < sortedCards.size(); ++i ){
				Card curCard = sortedCards.get(i);
				int diff = lastCard.getRank().getIntValue() - curCard.getRank().getIntValue(); 
				if ( diff != 1 ){
					isStraight = false; 
					break;
				} 
				lastCard = curCard;
			}
		} 
		
		if ( isStraight ){
			setHandTypeIfHigherRank(HandType.STRAIGHT, sortedCards);
		}
		
		return isStraight;
	}
	
	private boolean isFlush(){
		if ( isFlush != null ){
			return isFlush;
		}
		
		isFlush = false;
		
		// must have 5 entries - different ranks if they are all the same suit
		if ( groupedByRank.size() == cards.size() ){
			isFlush = true;
			Card firstCard = cards.get(0);
			for ( int i = 1; i < cards.size(); ++i ){
				if ( firstCard.getSuit() != cards.get(i).getSuit()){
					isFlush = false;
					break;
				}
			}
		}
		
		if ( isFlush ){
			setHandTypeIfHigherRank(HandType.FLUSH, cards);
		}
		
		return isFlush;
	}
}
