package com.gwtcrashcourse.letsplaypoker.engine;

import java.io.Serializable;


/**
 * Immutable Card type.
 * @author req77608
 *
 */
public class Card implements Comparable<Card>, Serializable{

	private static final long serialVersionUID = 4428333426441725255L;
	
	private Suit suit;
	private Rank rank;
	
	protected Card(){}
	
	public Card(Suit suit, Rank rank){
		this.suit = suit;
		this.rank = rank;
	}

	public Suit getSuit() {
		return suit;
	}

	public Rank getRank() {
		return rank;
	}
	
	protected void setSuit(Suit suit){
		this.suit = suit;
	}
	
	protected void setRank(Rank rank){
		this.rank = rank;
	}
	
	@Override
	public String toString(){
		return rank + "-" + suit;
	}

	@Override
	public int compareTo(Card otherCard) {
		// reverse standard number sorting -- Ace's first
		int result = rank.compareTo(otherCard.getRank()) * -1;
		if ( result == 0 ){
			result = suit.compareTo(otherCard.getSuit());
		} 
		return result;
	}
}
