package com.gwtcrashcourse.letsplaypoker.engine;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Immutable Hand representation that is returned from the Engine when dealing. 
 * 
 * The Hand has the cards that are in the hand, the wager that was made, and methods to manipulate the set of held cards.
 * 
 * Once you have set the cards that you want to hold, the Hand object will be passed back to the poker engine to draw.
 */
public class Hand implements Serializable {
	
	private static final long serialVersionUID = -747587258901229721L;

	private List<Card> cards;
	private List<Card> drawCards;
	private Set<Card> heldCards;
	private int wager;
	
	/* package-private */ 
	Hand(List<Card> cards, List<Card> drawCards, int wager, Set<Card> heldCards){
		this.cards = cards;
		this.drawCards = drawCards;
		this.wager = wager;
		this.heldCards = heldCards;
		if ( this.heldCards == null ){
			this.heldCards = new HashSet<Card>();
		}
	}
	
	/* package-private */
	Hand(){} // for GWT-RPC
	
	public List<Card> getCards(){
		return cards;
	}
	
	public int getWager(){
		return wager;
	}
	
	public boolean holdCard(Card c){
		return heldCards.add(c);
	}
	
	public boolean unholdCard(Card c){
		return heldCards.remove(c);
	}
	
	public boolean isHeld(Card c){
		return heldCards.contains(c);
	}
	
	public Set<Card> getHeldCards(){
		return heldCards;
	}
	
	/* package-private */
	List<Card> getDrawCards(){
		return drawCards;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for ( int i = 0; i < cards.size(); ++i ){
			if ( i > 0 ){
				sb.append(", ");
			}
			
			boolean held = heldCards.contains(cards.get(i));
			
			if ( held ){
				sb.append("[");
			}
			
			sb.append(i).append(": ").append(cards.get(i));
			
			if ( held ){
				sb.append("]");
			}
			
		}
		
		return sb.toString();
	}
	
}
