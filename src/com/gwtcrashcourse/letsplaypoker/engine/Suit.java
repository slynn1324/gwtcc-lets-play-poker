package com.gwtcrashcourse.letsplaypoker.engine;

/**
 * Playing Card Suits
 * 
 * @author req77608
 *
 */
@SuppressWarnings("javadoc")
public enum Suit {
	CLUB,
	DIAMOND,
	HEART,
	SPADE
}
