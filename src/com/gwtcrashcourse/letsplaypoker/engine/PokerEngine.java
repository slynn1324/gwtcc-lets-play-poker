package com.gwtcrashcourse.letsplaypoker.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Stateless PokerEnging for Jacks-Or-Higher 5-Card-Draw.
 * 
 * This is a rather naive implementation, but for the sake of example it works 
 * with a minimal interface, and very little to no attention to security.
 * 
 * @author req77608
 *
 */
public class PokerEngine {
	
	private static final int MAX_WAGER = 5;
	private static final int MIN_WAGER = 1;
	private static final int JACKPOT = 4000;

	// statically populated deck of 52 cards, ordered by rank and then suit 
	private static final List<Card> DECK = new ArrayList<Card>(52);
	static{
		for (Suit s : Suit.values()){
			for (Rank r : Rank.values()){
				DECK.add(new Card(s,r));
			}
		}
	}
	
	/**
	 * Deal a new hand. 
	 * 
	 * <p>
	 * Throws an IllegalArgumentException if the wager is not within the allowed bounds. 
	 * 
	 * @param wager - the number of credits to wager (1-5)
	 * @return
	 */
	public Hand deal(int wager){
		
		if ( wager < MIN_WAGER ){
			throw new IllegalArgumentException("You must wager at least " + MIN_WAGER + " credits.");
		} else if ( wager > MAX_WAGER ){
			throw new IllegalArgumentException("You may only wager up to " + MAX_WAGER + " credits.");
		}
		
		List<Card> shuffleDeck = new ArrayList<Card>(52);
		shuffleDeck.addAll(DECK);
		shuffle(shuffleDeck);
		
		List<Card> tenCards = shuffleDeck.subList(0, 10);
		
		List<Card> cards = new ArrayList<Card>(tenCards.subList(0, 5));
		List<Card> drawCards = new ArrayList<Card>(tenCards.subList(5,10));
		Hand h = new Hand(cards, drawCards, wager, null);
		return h;
	}
	
	/**
	 * Given the updated hand marking which cards are to be "held", deals a 
	 * FinalHand that includes the final cards, as well as the calculated HandType
	 * and payout amounts.
	 * 
	 * @param hand - the hand with cards optionally held
	 * @return the FinalHand which includes results
	 */
	public FinalHand draw(Hand hand){
		
		if ( hand instanceof FinalHand ){
			throw new IllegalArgumentException("You can't draw on a FinalHand.");
		}
		
		List<Card> newCards = new ArrayList<Card>(5);
				
		for ( int i = 0; i < 5; ++i ){
			Card origCard = hand.getCards().get(i);
			if ( hand.isHeld(origCard) ){
				newCards.add(hand.getCards().get(i));
			} else {
				newCards.add(hand.getDrawCards().get(i));
			}
		}
		
		NaiveHandEvaluator ev = new NaiveHandEvaluator(newCards);
		ev.score();
		
		int payout = getPayout(hand.getWager(), ev.getHandType(), ev.getCardsUsedInScore());
		
		FinalHand h = new FinalHand(newCards, hand.getWager(), hand.getHeldCards(), ev.getHandType(), ev.getCardsUsedInScore(), payout);
		return h;
	}
	
	private void shuffle(List<?> list) {
		Random rnd = new Random();
	    int size = list.size();
        for (int i=size; i>1; i--){
            Collections.swap(list, i-1, rnd.nextInt(i));
        }
	}
	
	
	private int getPayout(int wager, HandType handType, List<Card> cardsUsed){
		int payout = 0;
		if ( wager == MAX_WAGER && handType == HandType.ROYAL_FLUSH ){
			payout = JACKPOT;
		} else {

			if ( handType == HandType.ROYAL_FLUSH ){
				payout = 250 * wager;
			} else if (handType == HandType.STRAIGHT_FLUSH ){
				payout = 50 * wager;
			} else if ( handType == HandType.FOUR_OF_A_KIND ){
				payout = 25 * wager;
			} else if ( handType == HandType.FULL_HOUSE ){
				payout = 9 * wager;
			} else if ( handType == HandType.FLUSH ){
				payout = 6 * wager;
			} else if ( handType == HandType.STRAIGHT ){
				payout = 4 * wager;
			} else if ( handType == HandType.THREE_OF_A_KIND ){
				payout = 3 * wager;
			} else if ( handType == HandType.TWO_PAIR ){
				payout = 2 * wager;
			} else if ( handType == HandType.ONE_PAIR ){
				if ( cardsUsed.get(0).getRank().getIntValue() >= Rank.JACK.getIntValue() ){
					payout = 1 * wager;
				} 
			}
						
		}
		return payout;
	}
	
	public int getMaxBet(){
		return MAX_WAGER;
	}
	
	public int getMinBet(){
		return MIN_WAGER;
	}
}
