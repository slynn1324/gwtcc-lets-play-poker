package com.gwtcrashcourse.letsplaypoker.engine;

import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Subclass of Hand that is returned from the Engine's draw method.
 *
 * A final hand contains the hand, the cards used, the hand type, and the pay out. 
 */
public class FinalHand extends Hand{

	private static final long serialVersionUID = 6793966785703413819L;
	
	private HandType handType;
	private int payout;
	private List<Card> cardsUsed;
	
	FinalHand(){} // for GWT-RPC
	
	FinalHand(List<Card> cards, int wager, Set<Card> heldCards, HandType handType, List<Card> cardsUsed, int payout) {
		super(cards, Collections.<Card> emptyList(), wager, heldCards);
		this.handType = handType;
		this.cardsUsed = cardsUsed;
		this.payout = payout;
	}

	public HandType getHandType(){
		return handType;
	}
	
	public int getPayout(){
		return payout;
	}
	
	public List<Card> getCardsUsed(){
		return cardsUsed;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for ( int i = 0; i < getCards().size(); ++i ){
			if ( i > 0 ){
				sb.append(", ");
			}
			
			boolean held = getHeldCards().contains(getCards().get(i));
			boolean usedInScore = cardsUsed.contains(getCards().get(i));
			
			if ( usedInScore ){
				sb.append("**");
			}
			
			if ( held ){
				sb.append("[");
			}
			
			sb.append(i).append(": ").append(getCards().get(i));
			
			if ( held ){
				sb.append("]");
			}
			
			if ( usedInScore ){
				sb.append("**");
			}
			
		}
		
		return sb.toString();
	}
}
