package com.gwtcrashcourse.letsplaypoker.client;

import com.gwtcrashcourse.letsplaypoker.client.play.PlayView;


public class ClientFactory {

	public static final ClientFactory INSTANCE = new ClientFactory();
	
	private LaunchView launchView;
	private PlayView playView;
	private ApplicationView applicationView;
	
	public ClientFactory(){
		this.launchView = new LaunchView();
		this.playView = new PlayView();
		this.applicationView = new ApplicationView();
	}
	
	public LaunchView getLaunchView(){
		return launchView;
	}
	
	public PlayView getPlayView(){
		return playView;
	}
	
	public ApplicationView getApplicationView(){
		return applicationView;
	}
}
