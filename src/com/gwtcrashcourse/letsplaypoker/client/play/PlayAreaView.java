package com.gwtcrashcourse.letsplaypoker.client.play;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.gwtcrashcourse.letsplaypoker.engine.Card;
import com.gwtcrashcourse.letsplaypoker.engine.Hand;

public class PlayAreaView extends Composite {

	private static PlayAreaViewUiBinder uiBinder = GWT
			.create(PlayAreaViewUiBinder.class);

	interface PlayAreaViewUiBinder extends UiBinder<Widget, PlayAreaView> {
		// uib
	}

	@UiField CardSpot cardSpot1;
	@UiField CardSpot cardSpot2;
	@UiField CardSpot cardSpot3;
	@UiField CardSpot cardSpot4;
	@UiField CardSpot cardSpot5;
	
	private CardSpot[] cards;
	
	public PlayAreaView() {
		initWidget(uiBinder.createAndBindUi(this));
		// add cards to an array to simplify walking them
		cards = new CardSpot[5];
		cards[0] = cardSpot1;
		cards[1] = cardSpot2;
		cards[2] = cardSpot3;
		cards[3] = cardSpot4;
		cards[4] = cardSpot5;
	}
	
	public void setHand(Hand hand){
		for ( int i = 0; i < 5; ++i ){
			cards[i].setCard(hand.getCards().get(i));
		}
	}
	
	public boolean isCardHeld(int i){
		if ( i < 0 || i >= 5 ){
			throw new IndexOutOfBoundsException("There are only 5 cards.");
		}
		return cards[i].isHeld(); 
	}
	
	public void unholdAllCards(){
		for (CardSpot c : cards ){
			c.setHeld(false);
		}
	}
	
	public void setHoldEnabled(boolean enabled){
		for (CardSpot c : cards ){
			c.setHoldEnabled(enabled);
		}
	}
	
	public void resetCards(){
		for (CardSpot c : cards ){
			c.setCard(null);
		}
	}

}
