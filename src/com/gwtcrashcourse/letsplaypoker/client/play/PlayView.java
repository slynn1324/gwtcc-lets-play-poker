package com.gwtcrashcourse.letsplaypoker.client.play;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Widget;
import com.gwtcrashcourse.letsplaypoker.client.SimpleCallback;
import com.gwtcrashcourse.letsplaypoker.client.play.event.BetEvent;
import com.gwtcrashcourse.letsplaypoker.client.play.event.DealEvent;
import com.gwtcrashcourse.letsplaypoker.client.play.event.DrawEvent;
import com.gwtcrashcourse.letsplaypoker.client.play.event.ExitEvent;
import com.gwtcrashcourse.letsplaypoker.client.play.event.PayoutTableCloseEvent;
import com.gwtcrashcourse.letsplaypoker.client.play.event.ViewPayoutEvent;
import com.gwtcrashcourse.letsplaypoker.engine.Hand;

public class PlayView extends Composite {

	private static PlayViewUiBinder uiBinder = GWT
			.create(PlayViewUiBinder.class);

	interface PlayViewUiBinder extends UiBinder<Widget, PlayView> { /* marker */
	}

	private PlayController controller;
	
	@UiField PlayAreaView playArea;
	@UiField Header header;
	@UiField ActionArea actionArea;
	@UiField Footer footer;
	
	PayoutTableView payoutTable;
	DialogBox payoutTableDialog;
	
	public PlayView() {
		initWidget(uiBinder.createAndBindUi(this));
		
		payoutTable = new PayoutTableView();
		payoutTable.addPayoutTableCloseEventHandler(new PayoutTableCloseEvent.Handler(){
			@Override
			public void onClose(PayoutTableCloseEvent event) {
				payoutTableDialog.hide();
			}
		});
		payoutTableDialog = new DialogBox(true, true);
		payoutTableDialog.setAnimationEnabled(true);
		payoutTableDialog.setWidget(payoutTable);
	}
	
	public void setController(PlayController controller){
		this.controller = controller;
	}

	@UiHandler("actionArea")
	void onDeal(DealEvent e){
		controller.deal();
	}
	
	@UiHandler("actionArea")
	void onBet(BetEvent e){
		controller.bet(e.getBetType());
	}
	
	@UiHandler("actionArea")
	void onDraw(DrawEvent e){
		controller.draw();
	}
	
	@UiHandler("footer")
	void onExit(ExitEvent e){
		controller.exit();
	}
	
	@UiHandler("footer")
	void onViewPayout(ViewPayoutEvent e){
		controller.viewPayout();
	}
	
	public void showPayout(){
		payoutTableDialog.center();
	}
	
	public void setBet(int bet){
		header.setBet(bet);
	}
	
	public void setDealEnabled(boolean enabled){
		actionArea.setDealEnabled(enabled);
	}
	
	public void setDrawEnabled(boolean enabled){
		actionArea.setDrawEnabled(enabled);
	}
	
	public void setBetEnabled(boolean enabled){
		actionArea.setBetEnabled(enabled);
	}
	
	public void setCredits(int credits){
		header.setCredits(credits);
	}
	
	public void setHand(Hand hand){
		playArea.setHand(hand);
	}
	
	public boolean isCardHeld(int cardIndex){
		return playArea.isCardHeld(cardIndex);
	}
	
	public void setMessage(String msg){
		header.setMessage(msg);
	}
	
	public void unholdAllCards(){
		playArea.unholdAllCards();
	}
	
	public void setHoldEnabled(boolean enabled){
		playArea.setHoldEnabled(enabled);
	}
	
	public void showGameOver(SimpleCallback<Void> c){
		Window.alert("You're Broke! Game Over.");
		c.onComplete(null);
	}
	
	public void resetCards(){
		playArea.resetCards();
	}
	
}
