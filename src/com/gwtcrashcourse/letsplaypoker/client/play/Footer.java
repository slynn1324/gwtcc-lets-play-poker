package com.gwtcrashcourse.letsplaypoker.client.play;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.gwtcrashcourse.letsplaypoker.client.play.event.ExitEvent;
import com.gwtcrashcourse.letsplaypoker.client.play.event.ViewPayoutEvent;

public class Footer extends Composite {

	private static FooterUiBinder uiBinder = GWT.create(FooterUiBinder.class);

	interface FooterUiBinder extends UiBinder<Widget, Footer> {
		// uib
	}

	@UiField Button exit;
	@UiField Button viewPayout;
	
	public Footer() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public HandlerRegistration addExitEventHandler(ExitEvent.Handler h){
		return addHandler(h, ExitEvent.TYPE);
	}
	
	public HandlerRegistration addViewPayoutEventHandler(ViewPayoutEvent.Handler h){
		return addHandler(h, ViewPayoutEvent.TYPE);
	}
	
	@UiHandler("exit")
	void onExitClick(ClickEvent e){
		fireEvent(new ExitEvent());
	}
	
	@UiHandler("viewPayout")
	void onViewPayoutClick(ClickEvent e){
		fireEvent(new ViewPayoutEvent());
	}

}
