package com.gwtcrashcourse.letsplaypoker.client.play;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.gwtcrashcourse.letsplaypoker.client.play.event.BetEvent;
import com.gwtcrashcourse.letsplaypoker.client.play.event.DealEvent;
import com.gwtcrashcourse.letsplaypoker.client.play.event.DrawEvent;

public class ActionArea extends Composite {

	private static ActionAreaUiBinder uiBinder = GWT
			.create(ActionAreaUiBinder.class);

	interface ActionAreaUiBinder extends UiBinder<Widget, ActionArea> {
		// uib
	}

	@UiField Button betOne;
	@UiField Button betMax;
	@UiField Button draw;
	@UiField Button deal;
	
	public ActionArea() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public HandlerRegistration addDealEventHander(DealEvent.Handler h){
		return addHandler(h, DealEvent.TYPE);
	}
	
	public HandlerRegistration addBetEventHandler(BetEvent.Handler h){
		return addHandler(h, BetEvent.TYPE);
	}
	
	public HandlerRegistration addDrawEventHandler(DrawEvent.Handler h){
		return addHandler(h, DrawEvent.TYPE);
	}
	
	@UiHandler("betOne")
	void onBetOneClick(ClickEvent e){
		fireEvent(new BetEvent(BetEvent.BetType.BET_ONE));
	}
	
	@UiHandler("betMax")
	void onBetMaxClick(ClickEvent e){
		fireEvent(new BetEvent(BetEvent.BetType.BET_MAX));
	}
	
	@UiHandler("deal")
	void onDealClick(ClickEvent e){
		fireEvent(new DealEvent());
	}
	
	@UiHandler("draw")
	void onDrawClick(ClickEvent e){
		fireEvent(new DrawEvent());
	}
	
	public void setBetEnabled(boolean enabled){
		betOne.setEnabled(enabled);
		betMax.setEnabled(enabled);
	}
	
	public void setDealEnabled(boolean enabled){
		deal.setEnabled(enabled);
	}
	
	public void setDrawEnabled(boolean enabled){
		draw.setEnabled(enabled);
	}


}
