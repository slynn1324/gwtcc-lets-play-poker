package com.gwtcrashcourse.letsplaypoker.client.play;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.gwtcrashcourse.letsplaypoker.client.play.event.PayoutTableCloseEvent;

public class PayoutTableView extends Composite {

	private static PayoutTableViewUiBinder uiBinder = GWT
			.create(PayoutTableViewUiBinder.class);

	interface PayoutTableViewUiBinder extends UiBinder<Widget, PayoutTableView> {
	}
	
	@UiField Button close;

	public PayoutTableView() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@UiHandler("close")
	void onCloseClick(ClickEvent e){
		fireEvent(new PayoutTableCloseEvent());
	}
	
	public HandlerRegistration addPayoutTableCloseEventHandler(PayoutTableCloseEvent.Handler h){
		return addHandler(h, PayoutTableCloseEvent.TYPE);
	}

}
