package com.gwtcrashcourse.letsplaypoker.client.play;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.gwtcrashcourse.letsplaypoker.client.ClientFactory;
import com.gwtcrashcourse.letsplaypoker.client.LaunchController;
import com.gwtcrashcourse.letsplaypoker.client.SimpleCallback;
import com.gwtcrashcourse.letsplaypoker.client.play.event.BetEvent.BetType;
import com.gwtcrashcourse.letsplaypoker.engine.FinalHand;
import com.gwtcrashcourse.letsplaypoker.engine.Hand;
import com.gwtcrashcourse.letsplaypoker.engine.PokerEngine;

public class PlayController {
	
	private PlayView view;
	private AcceptsOneWidget panel;
	private PokerEngine pokerEngine = new PokerEngine();
	
	private int credits = 10;
	private int bet = 0;
	private Hand hand;

	public PlayController(PlayView view, int credits){
		this.view = view;
		this.credits = credits;
		bind();
		resetGame();
	}
	
	protected void bind(){
		view.setController(this);
	}
	
	protected void resetGame(){
		hand = null;
		view.setCredits(credits);
		view.unholdAllCards();
		view.resetCards();
		view.setHoldEnabled(false);
		resetBet();
		view.setMessage("Let&#8217;s Play Poker!<br />Make your bet to play");
	}

	public void start(AcceptsOneWidget panel){
		this.panel = panel;
		panel.setWidget(view);
	}
	
	public void stop(){
		LaunchController lc = new LaunchController(ClientFactory.INSTANCE.getLaunchView());
		lc.start(panel);
	}
	
	public void deal(){
		GWT.log("deal");
		view.unholdAllCards();
		hand = pokerEngine.deal(bet);
		GWT.log("hand=" + hand);
		updateHand();
		view.setDrawEnabled(true);
		view.setDealEnabled(false);
		view.setMessage("Choose cards to hold.");
		view.setHoldEnabled(true);
	}
	
	
	public void bet(BetType type){
		int maxBet = Math.min(credits, pokerEngine.getMaxBet());
		if ( type == BetType.BET_MAX ){
			credits = credits - (maxBet - bet);
			bet = maxBet;
		} else {
			++bet;
			--credits;
		}
		updateCredits();
		updateBet();
		if ( bet < maxBet ){
			view.setMessage("You can bet more!<br />..or click Deal");
		} else {
			view.setMessage("Maximum Bet Placed<br />Click Deal!");
		}
	}
	
	public void draw(){
		holdCards();
		hand = pokerEngine.draw(hand);
		view.setHoldEnabled(false);
		updateHand();
		payout();
		resetBet();
		showResult();
	}
	
	public void resetBet(){
		bet = 0;
		view.setBet(0);
		view.setBetEnabled(true);
		view.setDealEnabled(false);
		view.setDrawEnabled(false);
	}
	
	public void showResult(){
		if ( hand instanceof FinalHand ){
			FinalHand fh = (FinalHand) hand;
			String msg = fh.getHandType().getNiceString() + " pays out " + fh.getPayout() + "<br />Bet to play again.";
			view.setMessage(msg);
			
			if ( credits == 0 ){
				view.showGameOver(new SimpleCallback<Void>(){

					@Override
					public void onComplete(Void t) {
						exit();
					}
					
				});
			}
		}
	}

	public void payout(){
		if ( hand instanceof FinalHand ){
			FinalHand fh = (FinalHand) hand;
			credits += fh.getPayout();
			updateCredits();
		}
	}
	
	protected void holdCards(){
		for ( int i = 0; i < 5; ++i ){
			if ( view.isCardHeld(i) ){
				hand.holdCard(hand.getCards().get(i));
			}
		}
	}
	
	protected void updateHand(){
		view.setHand(hand);
	}
	
	protected void updateCredits(){
		view.setCredits(credits);
		if ( credits == 0 ){
			view.setBetEnabled(false);
		}
	}
	
	protected void updateBet(){
		view.setBet(bet);
		if ( bet == pokerEngine.getMaxBet() ){
			view.setBetEnabled(false);
		}
		if ( bet > 0){
			view.setDealEnabled(true);
		}  
	}
	
	protected void exit(){
		LaunchController c = new LaunchController(ClientFactory.INSTANCE.getLaunchView());
		c.start(panel);
	}
	
	public void viewPayout(){
		view.showPayout();
	}
}
