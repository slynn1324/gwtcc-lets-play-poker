package com.gwtcrashcourse.letsplaypoker.client.play.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public class BetEvent extends GwtEvent<BetEvent.Handler>{
	public interface Handler extends EventHandler{
		void onBet(BetEvent event);
	}
	
	public static final Type<Handler> TYPE = new Type<Handler>();
	
	@Override
	public Type<Handler> getAssociatedType(){
		return TYPE;
	}
	
	@Override
	protected void dispatch(Handler handler){
		handler.onBet(this);
	}
	
	/* custom data */
	
	public enum BetType{
		BET_ONE, BET_MAX;
	}
	
	private BetType betType;
	
	public BetEvent(BetType betType){
		this.betType = betType;
	}
	
	public BetType getBetType(){
		return betType;
	}
}
