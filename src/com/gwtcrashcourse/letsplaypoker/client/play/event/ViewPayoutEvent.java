package com.gwtcrashcourse.letsplaypoker.client.play.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public class ViewPayoutEvent extends GwtEvent<ViewPayoutEvent.Handler>{
	public interface Handler extends EventHandler{
		void onDeal(ViewPayoutEvent event);
	}
	
	public static final Type<Handler> TYPE = new Type<Handler>();
	
	@Override
	public Type<Handler> getAssociatedType(){
		return TYPE;
	}
	
	@Override
	protected void dispatch(Handler handler){
		handler.onDeal(this);
	}
}
