package com.gwtcrashcourse.letsplaypoker.client.play;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtcrashcourse.letsplaypoker.engine.Card;
import com.gwtcrashcourse.letsplaypoker.engine.widget.CardView;

public class CardSpot extends Composite {

	private static CardSpotUiBinder uiBinder = GWT
			.create(CardSpotUiBinder.class);

	interface CardSpotUiBinder extends UiBinder<Widget, CardSpot> {
		// uib
	}
	
	interface Style extends CssResource{
		String hold();
	}
	
	@UiField Style style;
	
	@UiField FocusPanel cardFocusWrap;
	@UiField CardView card;
	@UiField Button holdButton;
	
	boolean held = false;
	boolean enabled = true;
	
	public CardSpot() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void setCard(Card c){
		card.setCard(c);
	}
	
	@UiHandler("holdButton")
	void onHoldButtonClick(ClickEvent e){
		toggleHold();
	}
	
	@UiHandler("cardFocusWrap")
	void onCardFocusWrapClick(ClickEvent e){
		// can't stop this, so we add logic to stop
		if ( enabled ){
			toggleHold();
		}
	}
	
	protected void toggleHold(){
		held = !held;
		update();
	}
	
	public void update(){
		if ( held ){
			this.addStyleName(style.hold());
		} else {
			this.removeStyleName(style.hold());
		}
	}
	
	public boolean isHeld(){
		return held;
	}
	
	public void setHeld(boolean held){
		this.held = held;
		update();
	}
	
	public void setHoldEnabled(boolean enabled){
		this.enabled = enabled;
		this.holdButton.setEnabled(enabled);
	}
	
	
}
