package com.gwtcrashcourse.letsplaypoker.client.play;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class Header extends Composite {

	private static HeaderUiBinder uiBinder = GWT.create(HeaderUiBinder.class);

	interface HeaderUiBinder extends UiBinder<Widget, Header> {
		// uib
	}

	@UiField Label bet;
	@UiField Label credits;
	@UiField HTML message;
	
	public Header() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void setBet(int bet){
		this.bet.setText(Integer.toString(bet));
	}
	
	public void setCredits(int credits){
		this.credits.setText(Integer.toString(credits));
	}
	
	public void setMessage(String msg){
		message.setHTML(msg);
	}

}
