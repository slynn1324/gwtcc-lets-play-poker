package com.gwtcrashcourse.letsplaypoker.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class LaunchView extends Composite  {

	private static LaunchViewUiBinder uiBinder = GWT
			.create(LaunchViewUiBinder.class);

	interface LaunchViewUiBinder extends UiBinder<Widget, LaunchView> { /* marker */ }

	@UiField Label balance;
	
	@UiField Button begin;
	
	@UiField Button add5;

	@UiField Button add10;
	
	@UiField Button add25;
	
	LaunchController controller;
	
	public LaunchView() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void setController(LaunchController controller){
		this.controller = controller;
	}
	
	@UiHandler("begin")
	void onBeginClick(ClickEvent e){
		controller.begin();
	}
	
	@UiHandler("add5")
	void onAdd5Click(ClickEvent e){
		controller.add(5);
	}
	
	@UiHandler("add10")
	void onAdd10Click(ClickEvent e){
		controller.add(10);
	}
	
	@UiHandler("add25")
	void onAdd25Click(ClickEvent e){
		controller.add(25);
	}
	
	public void setBalance(int balance){
		this.balance.setText(Integer.toString(balance));
	}
}
