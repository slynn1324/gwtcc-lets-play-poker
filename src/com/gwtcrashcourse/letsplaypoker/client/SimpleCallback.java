package com.gwtcrashcourse.letsplaypoker.client;

public interface SimpleCallback<T> {
	public void onComplete(T t);
}
