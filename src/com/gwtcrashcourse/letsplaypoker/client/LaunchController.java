package com.gwtcrashcourse.letsplaypoker.client;

import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.gwtcrashcourse.letsplaypoker.client.play.PlayController;

public class LaunchController {

	private LaunchView view;
	private AcceptsOneWidget panel;
	
	int balance = 0;
	
	public LaunchController(LaunchView view){
		this.view = view;
		bind();
		resetView();
	}
	
	protected void bind(){
		view.setController(this);
	}
	
	protected void resetView(){
		view.setBalance(0);
	}
	
	public void start(AcceptsOneWidget panel){
		this.panel = panel;
		panel.setWidget(view);
	}
	
	public void begin(){
		PlayController playController = new PlayController(ClientFactory.INSTANCE.getPlayView(), balance);
		playController.start(panel);
	}
	
	public void add(int amount){
		balance += amount;
		updateBalance();
	}
	
	protected void updateBalance(){
		view.setBalance(balance); 
	}
}
