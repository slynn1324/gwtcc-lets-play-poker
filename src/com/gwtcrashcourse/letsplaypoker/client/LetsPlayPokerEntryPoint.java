package com.gwtcrashcourse.letsplaypoker.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class LetsPlayPokerEntryPoint implements EntryPoint {
	
	/**
	 * This is the entry point method.
	 */
	@Override
	public void onModuleLoad() {
		
		
		RootPanel.get().add(ClientFactory.INSTANCE.getApplicationView());
		
		LaunchView launchView = ClientFactory.INSTANCE.getLaunchView();
		LaunchController launchController = new LaunchController(launchView);
		launchController.start(ClientFactory.INSTANCE.getApplicationView());
		
		//PlayController playController = new PlayController(ClientFactory.INSTANCE.getPlayView());
		//playController.start(ClientFactory.INSTANCE.getApplicationView());
		
	}
}
