package com.gwtcrashcourse.letsplaypoker.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Just wrap the whole application in case we want to develop anything around the whole app.
 * 
 * We implement "AcceptsOneWidget" so that we can pass this widget into the controllers.
 * @author req77608
 *
 */
public class ApplicationView extends Composite implements AcceptsOneWidget {

	private static ApplicationViewUiBinder uiBinder = GWT
			.create(ApplicationViewUiBinder.class);

	interface ApplicationViewUiBinder extends UiBinder<Widget, ApplicationView> {
		// uib
	}
	
	/*
	interface Style extends CssResource{
		String pullLeft();
		String pullRight();
		String clear();
	}
	
	@UiField Style style;
	*/

	@UiField SimplePanel thePanel;
	
	public ApplicationView() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setWidget(IsWidget w) {
		thePanel.setWidget(w);
	}


}
