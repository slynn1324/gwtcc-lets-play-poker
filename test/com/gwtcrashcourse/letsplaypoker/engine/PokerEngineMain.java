package com.gwtcrashcourse.letsplaypoker.engine;

import java.io.IOException;
import java.util.Scanner;

/**
 * Example console runner for the PokerEngine
 * 
 * @author req77608
 *
 */
public class PokerEngineMain {
	public static void main(String[] args) throws IOException {

		boolean keepPlaying = true;
		Scanner scanner = new Scanner(System.in);
		PokerEngine p = new PokerEngine();
		
		int balance = 10;

		System.out.println("Welcome to Lets Play Poker!");
		System.out.println("Your starting balance is " + balance + " credits.");
		
		
		while (keepPlaying) {
			
			System.out.println();
			
			int wager = -1;
			
			while ( wager < 0 ){
				try{
					System.out.print("Wager [1-5]: ");
					String newWagerStr = scanner.next();
					if (newWagerStr.matches("^[0-9]+$") ){
						int newWager = Integer.parseInt(newWagerStr);
						wager = newWager;
					} else {
						System.out.println("Unrecognized number.");
					}
				} catch (IllegalArgumentException e){
					System.out.println(e.getMessage());
				}
			}

			Hand h = p.deal(wager);

			System.out.println();
			System.out.println("Your Hand:");
			System.out.println(h);

			boolean doneHolding = false;
			while (!doneHolding) {

				System.out.print("Card to hold, or D to re-deal: ");
				String input = scanner.next();
				if (input.matches("^[0-9]$")) {
					int idx = Integer.parseInt(input);
					Card c = h.getCards().get(idx);
					boolean held = h.holdCard(c);
					// if we didn't hold it, then it must already be held, let's
					// toggle
					if (!held) {
						h.unholdCard(c);
					}
					System.out.println("Holding card: " + c);
					System.out.println();
					System.out.println(h);
				} else if (input.matches("^d|D$")) {
					doneHolding = true;
				} else {
					System.out.println("Unknown command: " + input);
				}

			}

			FinalHand fh = p.draw(h);

			System.out.println();
			System.out.println(fh);
			System.out.println("Your hand is a: " + fh.getHandType());
			System.out.println("This hand pays out: " + fh.getPayout());
			
			balance += fh.getPayout();
			
			System.out.println("Your balance is now: " + balance);
			System.out.println();
			
			
			if( balance <= 0 ){
				System.out.println("Your balance is now 0. Thanks for your donation!");
				keepPlaying = false;
			} else {

				String playAgain = null;
	
				while (playAgain == null) {
					System.out.print("Play Again? [y|n]");
					
					String readPlayAgain = scanner.next();
					
					if ( readPlayAgain.matches("^y|Y|n|N$") ){
						playAgain = readPlayAgain;
					}
				}
	
				if (playAgain.matches("^n|N$")) {
					keepPlaying = false;
				}
			}

		}

		scanner.close();
	}
}
