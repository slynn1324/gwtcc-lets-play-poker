package com.gwtcrashcourse.letsplaypoker.engine;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

public class NaiveHandEvaluatorTest {

	@Test
	public void shouldBeRoyalFlush(){
		Hand h = hand(
				Suit.SPADE, Rank.ACE,
				Suit.SPADE, Rank.KING,
				Suit.SPADE, Rank.QUEEN,
				Suit.SPADE, Rank.JACK,
				Suit.SPADE, Rank.TEN
				);
		
		
		NaiveHandEvaluator n = score(h);
		assertEquals(HandType.ROYAL_FLUSH, n.getHandType());
		assertEquals(5, n.getCardsUsedInScore().size());
	}
	
	@Test
	public void shouldBeRoyalFlushWithDiamonds(){
		Hand h = hand(
				Suit.DIAMOND, Rank.ACE,
				Suit.DIAMOND, Rank.KING,
				Suit.DIAMOND, Rank.QUEEN,
				Suit.DIAMOND, Rank.JACK,
				Suit.DIAMOND, Rank.TEN
				);
		
		NaiveHandEvaluator n = score(h);
		assertEquals(HandType.ROYAL_FLUSH, n.getHandType());
		assertEquals(5, n.getCardsUsedInScore().size());
	}
	
	@Test
	public void shouldBeStraightFlush(){
		Hand h = hand(
				Suit.DIAMOND, Rank.KING,
				Suit.DIAMOND, Rank.QUEEN,
				Suit.DIAMOND, Rank.JACK,
				Suit.DIAMOND, Rank.TEN,
				Suit.DIAMOND, Rank.NINE
				);
		
		NaiveHandEvaluator n = score(h);
		
		assertEquals(HandType.STRAIGHT_FLUSH, n.getHandType());
		assertEquals(5, n.getCardsUsedInScore().size());
	}
	
	
	@Test
	public void shouldBeStraight(){
		Hand h = hand(
				Suit.SPADE, Rank.KING,
				Suit.DIAMOND, Rank.QUEEN,
				Suit.DIAMOND, Rank.JACK,
				Suit.DIAMOND, Rank.TEN,
				Suit.DIAMOND, Rank.NINE
				);
		
		NaiveHandEvaluator n = score(h);
		
		assertEquals(HandType.STRAIGHT, n.getHandType());
		assertEquals(5, n.getCardsUsedInScore().size());
	}
	
	@Test
	public void shouldBeFlush(){
		Hand h = hand(
				Suit.DIAMOND, Rank.KING,
				Suit.DIAMOND, Rank.TEN,
				Suit.DIAMOND, Rank.DEUCE,
				Suit.DIAMOND, Rank.THREE,
				Suit.DIAMOND, Rank.FIVE
				);
		
		NaiveHandEvaluator n = score(h);
		
		assertEquals(HandType.FLUSH, n.getHandType());
		assertEquals(5, n.getCardsUsedInScore().size());
	}
	
	
	@Test
	public void shouldBeFourOfAKind(){
		Hand h = hand(
				Suit.DIAMOND, Rank.KING,
				Suit.SPADE, Rank.KING,
				Suit.HEART, Rank.KING,
				Suit.CLUB, Rank.KING,
				Suit.DIAMOND, Rank.FIVE
				);
		
		NaiveHandEvaluator n = score(h);
		
		assertEquals(HandType.FOUR_OF_A_KIND, n.getHandType());
		assertEquals(4, n.getCardsUsedInScore().size());
	}
	
	@Test
	public void shouldBeFullHouse(){
		Hand h = hand(
				Suit.DIAMOND, Rank.KING,
				Suit.SPADE, Rank.KING,
				Suit.HEART, Rank.KING,
				Suit.SPADE, Rank.FIVE,
				Suit.DIAMOND, Rank.FIVE
				);
		
		NaiveHandEvaluator n = score(h);
		
		assertEquals(HandType.FULL_HOUSE, n.getHandType());
		assertEquals(5, n.getCardsUsedInScore().size());
	}
	
	@Test
	public void shouldBeThreeOfAKind(){
		Hand h = hand(
				Suit.DIAMOND, Rank.KING,
				Suit.SPADE, Rank.KING,
				Suit.HEART, Rank.KING,
				Suit.SPADE, Rank.FIVE,
				Suit.DIAMOND, Rank.DEUCE
				);
		
		NaiveHandEvaluator n = score(h);
		
		assertEquals(HandType.THREE_OF_A_KIND, n.getHandType());
		assertEquals(3, n.getCardsUsedInScore().size());
	}
	
	@Test
	public void shouldBeTwoPair(){
		Hand h = hand(
				Suit.DIAMOND, Rank.KING,
				Suit.SPADE, Rank.KING,
				Suit.HEART, Rank.FIVE,
				Suit.SPADE, Rank.FIVE,
				Suit.DIAMOND, Rank.DEUCE
				);
		
		NaiveHandEvaluator n = score(h);
		
		assertEquals(HandType.TWO_PAIR, n.getHandType());
		assertEquals(4, n.getCardsUsedInScore().size());
	}
	
	@Test
	public void shouldBePair(){
		Hand h = hand(
				Suit.DIAMOND, Rank.JACK,
				Suit.SPADE, Rank.JACK,
				Suit.HEART, Rank.FIVE,
				Suit.SPADE, Rank.THREE,
				Suit.DIAMOND, Rank.DEUCE
				);
		
		NaiveHandEvaluator n = score(h);
		
		assertEquals(HandType.ONE_PAIR, n.getHandType());
		assertEquals(2, n.getCardsUsedInScore().size());
		assertEquals(Rank.JACK, n.getCardsUsedInScore().get(0).getRank());
		assertEquals(Rank.JACK, n.getCardsUsedInScore().get(1).getRank());
	}
	
	@Test
	public void shouldNotBePair(){
		Hand h = hand(
				Suit.DIAMOND, Rank.TEN,
				Suit.SPADE, Rank.NINE,
				Suit.HEART, Rank.FIVE,
				Suit.SPADE, Rank.THREE,
				Suit.DIAMOND, Rank.DEUCE
				);
		
		NaiveHandEvaluator n = score(h);
		
		assertEquals(HandType.HIGH_CARD, n.getHandType());
	}
	
	@Test
	public void shouldBeHighCard(){
		Hand h = hand(
				Suit.DIAMOND, Rank.TEN,
				Suit.SPADE, Rank.NINE,
				Suit.HEART, Rank.FIVE,
				Suit.SPADE, Rank.THREE,
				Suit.DIAMOND, Rank.DEUCE
				);
		
		NaiveHandEvaluator n = score(h);
		
		assertEquals(1, n.getCardsUsedInScore().size());
		assertEquals(Rank.TEN, n.getCardsUsedInScore().get(0).getRank());
	}
	
	@Test
	public void shouldBeTwoPairOfAcesAndQueensWithKingKicker(){
		Hand h = hand(
				Suit.SPADE, Rank.ACE,
				Suit.DIAMOND, Rank.KING,
				Suit.HEART, Rank.ACE,
				Suit.CLUB, Rank.QUEEN,
				Suit.DIAMOND, Rank.QUEEN
				);
		NaiveHandEvaluator n = score(h);
		
		assertEquals(HandType.TWO_PAIR, n.getHandType() );
		assertEquals(4, n.getCardsUsedInScore().size());
	}
	
	
	protected NaiveHandEvaluator score(Hand h){
		NaiveHandEvaluator n = new NaiveHandEvaluator(h.getCards());
		n.score();
		return n;
	}
	
	protected Hand hand(Suit s1, Rank r1, Suit s2, Rank r2, Suit s3, Rank r3, Suit s4, Rank r4, Suit s5, Rank r5){

		List<Card> cards = new ArrayList<Card>();
		cards.add(new Card(s1, r1));
		cards.add(new Card(s2, r2));
		cards.add(new Card(s3, r3));
		cards.add(new Card(s4, r4));
		cards.add(new Card(s5, r5));
		
		Hand h = new Hand(cards, cards, 5, null);
		return h;
				
	}
}
