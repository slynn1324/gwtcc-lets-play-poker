package com.gwtcrashcourse.letsplaypoker.engine;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class PokerEngineTest {

	@Test
	public void shouldDeal5Cards(){
		PokerEngine pokerEngine = new PokerEngine();
		Hand h = pokerEngine.deal(5);
		assertEquals(5, h.getCards().size());
	}
	
	@Test
	public void shouldDealUniqueCards(){
		PokerEngine pokerEngine = new PokerEngine();
		Hand h = pokerEngine.deal(5);
		Set<Card> cards = new HashSet<Card>();
		for ( int i = 0; i < 5; ++i ){
			cards.add(h.getCards().get(i));
		}
		assertEquals(5, cards.size());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldNotDrawOnFinalHand(){
		PokerEngine pokerEngine = new PokerEngine();
		FinalHand fh = new FinalHand();
		pokerEngine.draw(fh);
	}
	
	@Test
	public void shouldHave5CardsInFinalHand(){
		PokerEngine pe = new PokerEngine();
		Hand h = pe.deal(5);
		FinalHand fh = pe.draw(h);
		assertEquals(5, fh.getCards().size());
	}
	
	@Test
	public void shouldDrawFirstCard(){
		PokerEngine pokerEngine = new PokerEngine();
		Hand h = pokerEngine.deal(5);
		for ( int i = 1; i < 5; ++i ){
			h.holdCard(h.getCards().get(i));
		}
		FinalHand fh = pokerEngine.draw(h);
		assertNotSame(h.getCards().get(0), fh.getCards().get(0));
	}
}
